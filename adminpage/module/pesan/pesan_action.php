<?php
session_start();
include "../../config/database.php";
include "../../libraries/fungsi_waktu.php";

$nama_tabel = "pesan";

if($_POST['mod']=="hapusData")
{
	$id = $_POST['id'];
	
	$hapusData = mysql_query("DELETE FROM $nama_tabel WHERE id_pesan = '$id'");
		
	if($hapusData)
	{
		$aktivitas = "Hapus Pesan";
		$keterangan = mysql_real_escape_string("Menghapus 'Pesan' Pada Tabel '$nama_tabel'");
		$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
		
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Pesan Dihapus!');
		</script>
		";
	}
	else
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menghapus!');
		</script>
		";
	}
}

if($_POST['mod']=="hapusDataTerpilih")
{
	$data_terpilih = $_POST['data_terpilih'];
	
	$hapusDataTerpilih = mysql_query("DELETE FROM $nama_tabel WHERE id_pesan IN ($data_terpilih)");
		
	if($hapusDataTerpilih)
	{
		$aktivitas = "Hapus Pesan Terpilih";
		$keterangan = mysql_real_escape_string("Menghapus Pesan Terpilih Pada Tabel '$nama_tabel'");
		$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
		
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Pesan Dihapus!');
		</script>
		";
	}
	else
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menghapus!');
		</script>
		";
	}
}
?>