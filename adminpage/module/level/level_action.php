<?php
session_start();
include "../../config/database.php";
include "../../libraries/fungsi_waktu.php";

$nama_tabel = "level";

if($_POST['mod']=="simpanData")
{
	$nama_level = mysql_real_escape_string($_POST['nama_level']);
	$keterangan = mysql_real_escape_string($_POST['keterangan']);
	
	if(empty($_POST['nama_level']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Nama Level Tidak Boleh Kosong!');
		</script>
		";
	}
	else
	{
		$simpanData = mysql_query("INSERT INTO $nama_tabel (nama_level, keterangan, tanggal_ditambah, jam_ditambah, ditambah_oleh) VALUE ('$nama_level', '$keterangan', '$tanggal_sekarang', '$jam_sekarang', '$_SESSION[username]')");
		
		if($simpanData)
		{
			$aktivitas = "Tambah Level";
			$keterangan = mysql_real_escape_string("Menambahkan '$nama_level' Pada Tabel '$nama_tabel'");
			$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
			
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Level Ditambahkan!');
				$('#form').modal('hide');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menambahkan!');
			</script>
			";
		}
	}
}

if($_POST['mod']=="perbaruiData")
{
	$id = $_POST['id'];
	$nama_level = mysql_real_escape_string($_POST['nama_level']);
	$keterangan = mysql_real_escape_string($_POST['keterangan']);
	
	if(empty($_POST['nama_level']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Nama Tidak Boleh Kosong!');
		</script>
		";
	}
	else
	{
		$perbaruiData = mysql_query("UPDATE $nama_tabel SET nama_level = '$nama_level', keterangan = '$keterangan', tanggal_diperbarui = '$tanggal_sekarang', jam_diperbarui = '$jam_sekarang', diperbarui_oleh = '$_SESSION[username]' WHERE id = '$id'");
		
		if($perbaruiData)
		{
			$aktivitas = "Perbarui Level";
			$keterangan = mysql_real_escape_string("Memperbarui '$nama_level' Pada Tabel '$nama_tabel'");
			$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
		
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Level Diperbarui!');
				$('#form').modal('hide');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Memperbarui!');
			</script>
			";
		}
	}
}

if($_POST['mod']=="hapusData")
{
	$id = $_POST['id'];
	
	$data = mysql_query("SELECT nama_level FROM $nama_tabel WHERE id = '$id'");
	$ambilData = mysql_fetch_array($data);
	$field = $ambilData['nama_level'];
	
	$cekLevel = mysql_num_rows(mysql_query("SELECT id_level FROM user WHERE id_level = '$id'"));
	if($cekLevel > 0)
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Level Sedang Digunakan User!');
		</script>";
	}
	else
	{
		$hapusData = mysql_query("DELETE FROM $nama_tabel WHERE id = '$id'");
		
		if($hapusData)
		{
			$aktivitas = "Hapus Level";
			$keterangan = mysql_real_escape_string("Menghapus '$field' Pada Tabel '$nama_tabel'");
			$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
			$hapusAkses = mysql_query("DELETE FROM hak_akses WHERE id_level = '$id'");
		
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Level Dihapus!');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menghapus!');
			</script>
			";
		}
	}
}

if($_POST['mod']=="hapusDataTerpilih")
{
	$data_terpilih = $_POST['data_terpilih'];
	
	$cekLevel = mysql_num_rows(mysql_query("SELECT id_level FROM user WHERE id_level IN ($data_terpilih)"));
	if($cekLevel > 0)
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Level Sedang Digunakan User!');
		</script>";
	}
	else
	{
		$hapusDataTerpilih = mysql_query("DELETE FROM $nama_tabel WHERE id IN ($data_terpilih)");
		
		if($hapusDataTerpilih)
		{
			$aktivitas = "Hapus Level Terpilih";
			$keterangan = mysql_real_escape_string("Menghapus Level Terpilih Pada Tabel '$nama_tabel'");
			$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
			$hapusAkses = mysql_query("DELETE FROM hak_akses WHERE id_level IN ($data_terpilih)");
		
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Level Dihapus!');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menghapus!');
			</script>
			";
		}
	}
}

if($_POST['mod']=="simpanAkses")
{
	$id_level = $_POST['id_level'];
	$id_menu = $_POST['id_menu'];

	foreach($id_menu as $id_menu)
	{	
		$r = (isset($_POST['r'][$id_menu]) ? $_POST['r'][$id_menu] : 0);
		$s = (isset($_POST['s'][$id_menu]) ? $_POST['s'][$id_menu] : 0);
		$w = (isset($_POST['w'][$id_menu]) ? $_POST['w'][$id_menu] : 0);
		$u = (isset($_POST['u'][$id_menu]) ? $_POST['u'][$id_menu] : 0);
		$d = (isset($_POST['d'][$id_menu]) ? $_POST['d'][$id_menu] : 0);
		
		$cekData = mysql_num_rows(mysql_query("SELECT * FROM hak_akses WHERE id_level = '$id_level' AND id_menu = '$id_menu'"));
		if($cekData == 0)
		{
			$simpanData = mysql_query("INSERT INTO hak_akses (id_level, id_menu, r, s, w, u, d, tanggal_ditambah, jam_ditambah, ditambah_oleh) VALUE ('$id_level', '$id_menu', '$r', '$s', '$w', '$u', '$d', '$tanggal_sekarang', '$jam_sekarang', '$_SESSION[username]')");
			
			if($simpanData)
			{
				$aktivitas = "Tambah Akses";
				$keterangan = mysql_real_escape_string("Menambahkan Akses Pada 'id_level = $id_level' Pada Tabel 'Hak Akses'");
				$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
			
				echo "
				<script>
					alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Akses Ditambahkan!');
					$('#form').modal('hide');
				</script>
				";
			}
			else
			{
				echo "
				<script>
					alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menambahkan!');
				</script>
				";
			}
		}
		else
		{
			$perbaruiData = mysql_query("UPDATE hak_akses SET id_level = '$id_level', id_menu = '$id_menu', r = '$r', s = '$s', w = '$w', u = '$u', d = '$d', tanggal_diperbarui = '$tanggal_sekarang', jam_diperbarui = '$jam_sekarang', diperbarui_oleh = '$_SESSION[username]' WHERE id_level = '$id_level' AND id_menu = '$id_menu'");
			
			if($perbaruiData)
			{
				$aktivitas = "Perbarui Akses";
				$keterangan = mysql_real_escape_string("Memperbarui Akses Pada 'id_level = $id_level' Pada Tabel 'Hak Akses'");
				$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
			
				echo "
				<script>
					alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Akses Diperbarui!');
					$('#form').modal('hide');
				</script>
				";
			}
			else
			{
				echo "
				<script>
					alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Memperbarui!');
				</script>
				";
			}
		}
		
	}
}
?>