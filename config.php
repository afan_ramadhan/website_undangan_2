<?php
//konfigurasi database
include "adminpage/config/database.php";

 
//konfigurasi dasar
date_default_timezone_set('Asia/Jakarta');

$title = "Undangan Pernikahan Fulan Dan Fulanah";
$keywords = "Website Undangan Pernikahan, Website Undangan, Website Pernikahan, Undangan Pernikahan, Undangan Digital, Ketanware";
$description = "Website ini merupakan website Pernikahan Fulan dan Fulanah yang akan segera melakukan resepsi pernikahan. Silahkan berikan doa dan harapan untuk kami berdua.";
$author = "Afan Ramadhan";


function tanggal_indonesia($tanggal)
{
	$bulan = array (1 => "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
	$split = explode("-", $tanggal);
	
	return $split[2] . " " . $bulan[(int)$split[1]] . " " . $split[0];
}


//biodata pengantin 1
$pengantin_1 = "Fulan";
$profil_pengantin_1 = "Putra Pertama Bapak Abu Fulan Dan Ibu Ummu Fulan.";
$facebook_pengantin_1 = "https://www.facebook.com/";
$twitter_pengantin_1 = "https://twitter.com/";
$instagram_pengantin_1 = "https://www.instragram.com/";


//biodata pengantin 2
$pengantin_2 = "Fulanah";
$profil_pengantin_2 = "Putri Pertama Bapak Abu Fulanah Dan Ibu Ummu Fulanah.";
$facebook_pengantin_2 = "https://www.facebook.com/";
$twitter_pengantin_2 = "https://twitter.com/";
$instagram_pengantin_2 = "https://www.instragram.com/";


//konfigurasi undangan
$sambutan_undangan = "Bismillahirrahmanirrahim.. . ";
$tanggal = "01 Desember 2021";


$keluarga_ket = "Keluarga Besar Bapak Abu Fulan Dan Ibu Ummu Fulan";


$acara_ket = "Merupakan Suatu Kebahagiaan Bagi Kami Apabila Bapak / Ibu, Saudara / Saudari Berkenan Hadir Dan Memberikan Do'a Restu Kepada Kami. Kami Mengundang Anda Untuk Hadir Dalam Acara Pernikahan Kami";

$acara_1_ket = "Akad Nikah";
$tanggal_waktu_acara_1 = "01 Desember 2021<br/>Pukul 09:00 WIB - Selesai";
$lokasi_acara_1 = "Jl. Jend. Sudirman No.139, Kota Gapura, Kotabumi, Kabupaten Lampung Utara, Lampung<br/>(Kediaman Bapak Abu Fulan)";

$acara_2_ket = "Unduh Mantu";
$tanggal_waktu_acara_2 = "07 Desember 2021<br/>Pukul 10:00 WIB - Selesai";
$lokasi_acara_2 = "Jl. Jend. Sudirman No.139, Kota Gapura, Kotabumi, Kabupaten Lampung Utara, Lampung<br/>(Kediaman Bapak Abu Fulanah)";

$lokasi = "Jl. Jend. Sudirman No.139, Kota Gapura, Kotabumi, Kabupaten Lampung Utara, Lampung 34514";
$lokasi_src = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d31804.400594816885!2d104.89457402576902!3d-4.847031865408338!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e38a91ebbff8b93%3A0x952d3d939d091fa3!2sTugu%20Payan%20Mas!5e0!3m2!1sid!2sid!4v1618385921399!5m2!1sid!2sid";
$lokasi_href = "https://goo.gl/maps/SA7fP7niw5MiBod57";

$doa_ket = "Kami Akan Senang Jika Bapak / Ibu, Saudara / Saudari Berkenan Memberikan Ucapan Berupa Harapan Ataupun Do'a Untuk Kami.";


//other function
$penerima = (isset($_GET['kepada']) ? str_replace("+", " ", $_GET['kepada']) : "Undangan Yang Terhormat");
$alamat = (isset($_GET['di']) ? str_replace("+", " ", $_GET['di']) : "Tempat");
?>